# OpenSwimSoftware Install Scripts

Basic install scripts for various Opertating systems

## OS X

### setup-gitlab-ci-runner

Sets up a Mac to be a gitlab runner. 

Most likely for testing Objective-C builds

TODO: Modify to support testing Titanium iOS builds

## Ubuntu

### setup-base

Most basic Ubuntu setup

#### Software

* software-properties-common
* Node.js
* Grunt.js
* zram-config
* Git
* zsh

#### PPA

* chris-lea/node.js

### setup-user

Setup an Ubuntu user account

### setup-desktop

Setup Ubuntu Desktop

#### Software

* Restricted Extras
* Mir 
* Sublime Text 3 
* System Load Indicator
* Open in Terminal 
* GDebi 
* Hardware Info
* TeamViwer

#### PPA

* ubuntu-wine/ppa
* webupd8team/sublime-text-3
* libreoffice/ppa

### setup-docker

Install Docker.io

#### Software

* Docker.io
* Decking.io

### setup-nginx

Install Nginx with PageSpeed

#### Software

* Nginx
* PageSpeed

### setup-cleanup

Clean Up Apt

## License

MIT (with ABRMS amendment)