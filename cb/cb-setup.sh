#!/bin/bash
docker run -d --name cb --ulimit nofile=40960:40960 --ulimit core=100000000:100000000 --ulimit memlock=100000000:100000000 -p 8091-8094:8091-8094 -p 11210:11210 -p 8095:8095 --restart always couchbase
docker exec -t cb /bin/bash -c "wget http://packages.couchbase.com/releases/query-workbench/dp3/couchbase-query-workbench_dp3-linux_x86_64.tar.gz"

docker exec -t cb /bin/bash -c "tar -xvzf couchbase-query-workbench_dp3-linux_x86_64.tar.gz"
docker exec -t cb /bin/bash -c "rm couchbase-query-workbench_dp3-linux_x86_64.tar.gz"

docker exec -it cb /bin/bash -c "cd couchbase-query-workbench && ./launch-cbq-gui.sh"
