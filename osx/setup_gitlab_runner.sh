#!/bin/bash

brew install icu4c
brew update

brew install rbenv
brew install ruby-build
brew install openssl

brew tab homebrew/versions
brew install gcc48

CC=gcc-4.8 RUBY_CONFIGURE_OPTS="--with-openssl-dir=`brew --prefix openssl` --with-readline-dir=`brew --prefix readline` --with-gcc=gcc-4.8 --enable-shared" rbenv install 2.1.0
echo 'if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi' >> ~/.profile
rbenv global 2.1.0

sudo mkdir /Developer
cd /Developer

sudo gem install bundler

git clone https://gitlab.com/gitlab-org/gitlab-ci-runner.git
cd gitlab-ci-runner
sudo bundle install

ssh-keyscan -p 4022 -H home.openswimsoftware.com >> ~/.ssh/known_hosts

bundle exec ./bin/runner