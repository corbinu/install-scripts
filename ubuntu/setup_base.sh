#!/bin/bash

sudo sed -i "/^# deb .*partner/ s/^# //" /etc/apt/sources.list

sudo apt-get update

sudo add-apt-repository -y ppa:nginx/development
sudo add-apt-repository -y ppa:git-core/ppa

sudo apt-get update
sudo apt-get -y dist-upgrade

sudo apt-get install -y wget zram-config zsh git git-core curl zip unzip dkms

curl -L https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh | bash
