#!/bin/bash

sudo add-apt-repository -y ppa:ubuntu-wine/ppa
sudo add-apt-repository -y ppa:webupd8team/sublime-text-3
sudo add-apt-repository -y ppa:libreoffice/ppa
sudo add-apt-repository -y ppa:zedtux/naturalscrolling
sudo add-apt-repository -y ppa:ubuntu-mozilla-daily/ppa

sudo apt-get update
sudo apt-get -y dist-upgrade

sudo sh -c "echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections"

sudo apt-get install -y naturalscrolling sublime-text-installer indicator-multiload firefox-trunk libdvdread4 nautilus-open-terminal gdebi-core hardinfo
sudo apt-get remove -y firefox

sudo /usr/share/doc/libdvdread4/install-css.sh

wget http://download.teamviewer.com/download/teamviewer_linux.deb
sudo gdebi -n teamviewer_linux.deb
sudo rm teamviewer_linux.deb
