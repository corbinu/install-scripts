#!/bin/bash

sudo apt-get update

sudo apt-get install -y build-essential zlib1g-dev libpcre3 libpcre3-dev libssl-dev

sudo mkdir /tmp/nginx
cd /tmp/nginx
sudo wget https://github.com/pagespeed/ngx_pagespeed/archive/v1.7.30.3-beta.zip
sudo unzip v1.7.30.3-beta.zip
cd /tmp/nginx/ngx_pagespeed-1.7.30.3-beta/
sudo wget https://dl.google.com/dl/page-speed/psol/1.7.30.3.tar.gz
sudo tar -xzvf 1.7.30.3.tar.gz
cd /tmp/nginx
sudo wget http://nginx.org/download/nginx-1.5.10.tar.gz
sudo tar -xvzf nginx-1.5.10.tar.gz
cd /tmp/nginx/nginx-1.5.10/
sudo ./configure --with-http_ssl_module --with-http_spdy_module --add-module=/tmp/nginx/ngx_pagespeed-1.7.30.3-beta
sudo make
sudo make install

cd ~/
sudo rm -rf /tmp/nginx

sudo wget https://raw.github.com/JasonGiedymin/nginx-init-ubuntu/master/nginx -O /etc/init.d/nginx
sudo chmod +x /etc/init.d/nginx

sudo rm /usr/local/nginx/conf/nginx.conf
cd /usr/local/nginx/conf/
sudo wget https://bitbucket.org/corbinu/install-scripts/raw/master/ubuntu/config/nginx.conf

sudo mkdir -p /etc/nginx/conf.d
sudo mkdir -p /etc/nginx/sites-enabled
sudo mkdir -p /var/ngx_pagespeed_cache

sudo sh -c 'echo "tmpfs   /var/ngx_pagespeed_cache                        tmpfs   nodev,nosuid,size=20m          0        0" >> /etc/fstab'
sudo mount -a

sudo service nginx start

cd /etc/nginx/
sudo wget https://bitbucket.org/corbinu/install-scripts/raw/master/ubuntu/config/fastcgi_params

sudo apt-get remove -y build-essential zlib1g-dev libpcre3-dev libssl-dev
sudo apt-get -y autoremove